#Path to treefile - must be ultrametric
treeFile ####.newick

#Name of output file
outFile ####.simulatedTrees.txt

#Number of gene trees to simulate
nTrees 1000

#Require at least one leaf per species - no (0) yes (1)
force_minimum 1

###Fixed dup and loss params
fixed_lambda ####
fixed_mu ####

#Should bootstrap replicates be created from the simulated trees - no (0), yes (1)
bootStrap 0
#If yes, how many bootstrap replicates
nReps 100
#What is the distribution of gene family sizes at the root of the species tree?
#This sets the mean of a geometric distribution shifted to k={1,2,...,n}
root_distribution ####

#What is the retention rate of the WGD - set to 0 if there is no WGD
wgd_retention_rate ####
#Must be >0
wgd_time_before_divergence ####